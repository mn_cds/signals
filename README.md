# signals

pip-installable package with tools for signal data analysis. 

## Structure

<ul>
    <li> <b>process</b>: raw waveform analysis tools;
    <li> <b>visualize</b>: plot and visualization tools.
</ul>