import numpy as np
from tqdm.auto import tqdm
from scipy import interpolate


class signal:
    def __init__(self,*args,**kwargs):
        # Set default parameters
        self.time = []
        self.waveform = []
        self.sampling_frequency = 250
        self.timestep = 1/self.sampling_frequency
        self.timestep_resample = 1/self.sampling_frequency
        self.time0 = 0
        self.interp_kind = 'cubic'
        self.fft_configs = dict(window = 2,
                                overlap = 1,
                                band = [4,8])
        self.waveform_plot_configs = dict(figsize = (12,4),
                                          xmin = 0,
                                          xmax = 0,
                                          ymin = 0,
                                          ymax = 0,
                                          style = '.-',
                                          title = 'title',
                                          xlabel = 'xlabel',
                                          ylabel = 'ylabel',
                                          fontsize = 10)
        self.fft_frequencies = []
        self.fft_spectrum = []
        self.fft_band_power = []
        self.fft_time = []
        # Update with provided parameters
        self.__dict__.update(kwargs)


    def waveform_clean(self):
        '''
        If multiple (time,value) duplets with the same time exist, 
        only the first one is kept.
        '''
        redundancy_ixs = np.where(np.diff(self.time)==0)
        self.time = np.delete(self.time,redundancy_ixs)
        self.waveform = np.delete(self.waveform,redundancy_ixs)


    def waveform_resample(self):
        '''
        Signal resampling

        self.timestep_resample [float]: new timestep
        self.interp_kind [str]: interpolation method (see scipy.interpolate for options)
        '''
        interp_function = interpolate.interp1d(self.time,self.waveform,kind=self.interp_kind)
        self.time = np.arange(self.time[0],self.time[-1],self.timestep_resample)
        self.waveform = interp_function(self.time)
        self.timestep = self.timestep_resample
        

    def waveform_draw(self):
        '''
        Draws (time,waveform) curve
        '''
        fig,ax = plt.subplots(figsize=self.waveform_plot_configs['figsize'])

        ax.plot(self.time,self.waveform,self.waveform_plot_configs['style'])
        ax.set_xlabel(self.waveform_plot_configs['xlabel'])
        ax.set_ylabel(self.waveform_plot_configs['ylabel'])
        if(self.waveform_plot_configs['xmin']!=0 & self.waveform_plot_configs['xmax']!=0):
            ax.set_xlim(self.waveform_plot_configs['xmin'],self.waveform_plot_configs['xmax'])
        if(self.waveform_plot_configs['ymin']!=0 & self.waveform_plot_configs['ymax']!=0):
            ax.set_xlim(self.waveform_plot_configs['ymin'],self.waveform_plot_configs['ymax'])
        ax.set_xlabel(self.waveform_plot_configs['xlabel'])
        ax.set_ylabel(self.waveform_plot_configs['ylabel'])
        return fig

    
    def waveform_fft(self):
        '''
        Performs fast fourier transform on waveform using numpy.fft library
        '''
        window_n_samples = int(self.sampling_frequency*self.fft_configs['window'])
        overlap_n_samples = int(self.sampling_frequency*self.fft_configs['overlap'])
        for i in tqdm(range(window_n_samples,len(self.waveform)-window_n_samples,window_n_samples-overlap_n_samples)):
            this_waveform = self.waveform[i-overlap_n_samples:i-overlap_n_samples+window_n_samples]
            tp_count = len(this_waveform)
            values = np.arange(int(tp_count/2))
            time_period = tp_count/self.sampling_frequency
            frequencies = values/time_period
            fourier_transform = np.fft.fft(this_waveform)
            fourier_transform = fourier_transform[range(int(len(this_waveform)/2))] 
            this_spectrum = abs(fourier_transform)
            self.fft_spectrum.append(this_spectrum)
            self.fft_time.append(np.mean(self.time[i-overlap_n_samples:i-overlap_n_samples+window_n_samples]))
        self.fft_frequencies = frequencies


    def compute_band_power(self):
        '''
        Computes total power in a specific frequency band

        self.fft_band_power = [f_min, f_max]: frequency band
        self.fft_spectrum = array with fft spectra of different time windows
        '''
        self.fft_band_power = []
        ix_start = np.argmin(np.abs(self.fft_frequencies-self.fft_configs['band'][0]))
        ix_stop = np.argmin(np.abs(self.fft_frequencies-self.fft_configs['band'][1]))
        for i in tqdm(range(len(self.fft_spectrum))):    
            self.fft_band_power.append(sum(self.fft_spectrum[i][ix_start:ix_stop]))